import React from "react";

export default class Post extends React.Component {
  render() {
    return (
      <>
        <h3>Latest Post</h3>
        <p>
          {this.props.p} <a href="/">...</a>{" "}
          <span>{new Date().toLocaleTimeString()}</span>
        </p>
      </>
    );
  }
}
