import React from "react";
import Content from "../container/Content";
import Footer from "../container/Footer";
import Header from "../container/Header";

export default class Home extends React.Component {
  render() {
    return (
      <>
        <Header />
        <Content />
        <Footer />
      </>
    );
  }
}
