import React from "react";
import Adbox from "../component/adbox/Adbox";
import Title from "../component/adbox/Title";
import Body from "../component/body/Body";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

export default class Content extends React.Component {
  state = {
    imgs: [],
    articles: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/imghome");
    const body = await response.json();
    this.setState({ imgs: body });

    const resArticles = await fetch("http://localhost:8080/list/article");
    const bodyArticles = await resArticles.json();
    this.setState({ articles: bodyArticles });
  }

  render() {
    const { imgs } = this.state;
    const { articles } = this.state;
    return (
      <>
        <div id="boximg">
          <Title title="HOT SHIRTS FOR THIS MONTH" />
          <ul>
            <Carousel
              additionalTransfrom={0}
              arrows
              autoPlay
              autoPlaySpeed={1000}
              centerMode={false}
              className=""
              containerClass="container-with-dots"
              dotListClass=""
              draggable
              focusOnSelect={false}
              infinite
              itemClass=""
              keyBoardControl
              minimumTouchDrag={80}
              renderButtonGroupOutside={false}
              renderDotsOutside={false}
              responsive={{
                desktop: {
                  breakpoint: {
                    max: 3000,
                    min: 1024,
                  },
                  items: 3,
                  partialVisibilityGutter: 40,
                },
                mobile: {
                  breakpoint: {
                    max: 464,
                    min: 0,
                  },
                  items: 1,
                  partialVisibilityGutter: 30,
                },
                tablet: {
                  breakpoint: {
                    max: 1024,
                    min: 464,
                  },
                  items: 2,
                  partialVisibilityGutter: 30,
                },
              }}
              showDots={false}
              sliderClass=""
              slidesToSlide={1}
              swipeable
            >
              {imgs.map((img) => (
                <Adbox alt="image" src={img.imgurl} />
              ))}
            </Carousel>
          </ul>
        </div>
        <div id="bodyContent">
          <div id="contents">
            <ul id="articles">
            <Carousel
              additionalTransfrom={0}
              arrows
              autoPlay={true}
              autoPlaySpeed={2000}
              centerMode={false}
              className=""
              containerClass="container-with-dots"
              dotListClass=""
              draggable
              focusOnSelect={true}
              infinite
              itemClass=""
              keyBoardControl
              minimumTouchDrag={80}
              renderButtonGroupOutside={false}
              renderDotsOutside={false}
              responsive={{
                desktop: {
                  breakpoint: {
                    max: 3000,
                    min: 1024,
                  },
                  items: 3,
                  partialVisibilityGutter: 40,
                },
                mobile: {
                  breakpoint: {
                    max: 464,
                    min: 0,
                  },
                  items: 1,
                  partialVisibilityGutter: 30,
                },
                tablet: {
                  breakpoint: {
                    max: 1024,
                    min: 464,
                  },
                  items: 2,
                  partialVisibilityGutter: 30,
                },
              }}
              showDots={false}
              sliderClass=""
              slidesToSlide={1}
              swipeable
            >
              {articles.map((article) => (
                <Body
                  title={article.title}
                  p={article.p}
                  btnName={article.btnname}
                />
              ))}
              </Carousel>
            </ul>
          </div>
        </div>
      </>
    );
  }
}
