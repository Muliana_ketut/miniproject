import React from "react";
import FooterNote from "../component/footer/FooterNote";
import Medsos from "../component/footer/Medsos";
import Post from "../component/footer/Post";
import Subscribe from "../component/footer/Subscribe";

export default class Footer extends React.Component {
  render() {
    return (
      <>
        <div id="footer-p">
          <div className="background">
            <div className="body">
              <div className="subscribe">
                <Subscribe />
              </div>
              <div className="posts">
                <Post />
              </div>
              <div className="connect">
                <Medsos />
              </div>
            </div>
          </div>
          <FooterNote />
        </div>
      </>
    );
  }
}
