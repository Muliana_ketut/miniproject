import React from "react";
import Logo from "../component/header/Logo";
import Navigation from "../component/header/Navigation";

export default class Header extends React.Component {
  render() {
    return (
      <>
        <div id="header">
          <div>
            <Navigation />
            <Logo />
          </div>
        </div>
      </>
    );
  }
}
