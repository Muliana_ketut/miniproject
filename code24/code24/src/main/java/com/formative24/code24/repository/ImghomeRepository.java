package com.formative24.code24.repository;

import com.formative24.code24.modal.Imghome;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImghomeRepository extends JpaRepository<Imghome, Integer> {

}
